import os
from os.path import join
import json
import requests

dir_path = os.path.dirname(os.path.realpath(__file__))
keycloak_config_file = join(dir_path, "secrets.json")

d4p_host = os.environ["D4P_REGISTRY_PUBLIC_SERVICE_HOST"] if "D4P_REGISTRY_PUBLIC_SERVICE_HOST" in os.environ else \
    "localhost"
d4p_port = os.environ["D4P_REGISTRY_PUBLIC_SERVICE_PORT"] if "D4P_REGISTRY_PUBLIC_SERVICE_PORT" in os.environ else 8000
if d4p_host == "localhost":
    D4P_URL = "https://platform.dare.scai.fraunhofer.de/d4p-registry"
else:
    D4P_URL = "http://{}:{}".format(d4p_host, d4p_port)

exec_host = os.environ["EXEC_API_PUBLIC_SERVICE_HOST"] if "EXEC_API_PUBLIC_SERVICE_HOST" in os.environ else "localhost"
exec_port = os.environ["EXEC_API_PUBLIC_SERVICE_PORT"] if "EXEC_API_PUBLIC_SERVICE_PORT" in os.environ else 80
if exec_host == "localhost":
    EXEC_API_URL = "https://platform.dare.scai.fraunhofer.de/exec-api"
else:
    EXEC_API_URL = "http://{}:{}".format(exec_host, exec_port)

cwl_host = os.environ["WORKFLOW_REGISTRY_PUBLIC_SERVICE_HOST"] if "WORKFLOW_REGISTRY_PUBLIC_SERVICE_HOST" in \
                                                                  os.environ else "localhost"
cwl_port = os.environ["WORKFLOW_REGISTRY_PUBLIC_SERVICE_PORT"] if "WORKFLOW_REGISTRY_PUBLIC_SERVICE_PORT" in \
                                                                  os.environ else 8000
if cwl_host == "localhost":
    WORKFLOW_REGISTRY_URL = "https://platform.dare.scai.fraunhofer.de/workflow-registry"
else:
    WORKFLOW_REGISTRY_URL = "http://{}:{}".format(cwl_host, cwl_port)


def load_keycloak_config():
    """
    Reads json file with keycloak configuration. Client ID and secret can be provided as env variables in the
    kubernetes yaml files. If they are not available, they are loaded from the json configuration file.

    Returns
        | dict: keycloak configuration settings
    """
    with open(keycloak_config_file, 'r') as f:
        keycloak_conf = json.loads(f.read())
        base_path = os.environ["KEYCLOAK_DOMAIN"] if "KEYCLOAK_DOMAIN" in os.environ else \
            keycloak_conf["web"]["base_path"]
        keycloak_conf["web"]["base_path"] = base_path
        keycloak_conf["web"]["issuer"] = base_path + keycloak_conf["web"]["issuer"]
        keycloak_conf["web"]["auth_uri"] = base_path + keycloak_conf["web"]["auth_uri"]
        keycloak_conf["web"]["userinfo_uri"] = base_path + keycloak_conf["web"]["userinfo_uri"]
        keycloak_conf["web"]["users_uri"] = base_path + keycloak_conf["web"]["users_uri"]
        keycloak_conf["web"]["token_uri"] = base_path + keycloak_conf["web"]["token_uri"]
        keycloak_conf["web"]["token_introspection_uri"] = base_path + keycloak_conf["web"]["token_introspection_uri"]
        return keycloak_conf


def validate_token(token, full_resp=None):
    """
    Function to validate a given token. If the token is valid, the API asks for user info so as to retrieve a
    permanent id. Some of the returned fields (if token is valid) are as presented below:

    - "sub": "cfbdc618-1d1e-4b1d-9d35-3d23f271abe3",
    - "email_verified": true,
    - "name": "Sissy Themeli",
    - "preferred_username": "sthemeli",
    - "given_name": "Sissy",
    - "family_name": "Themeli",
    - "email": "sthemeli@iit.demokritos.gr"

    Args
        | request: http request
        | oidc: open-id client

    Returns
        | str: user's permanent id
    """
    keycloak_config = load_keycloak_config()
    client_id = os.environ["CLIENT_ID"] if "CLIENT_ID" in os.environ else keycloak_config["web"]["client_id"]
    client_secret = os.environ["CLIENT_SECRET"] if "CLIENT_SECRET" in os.environ else keycloak_config["web"][
        "client_secret"]
    headers = {"content-type": "application/x-www-form-urlencoded"}
    request_data = {"client_id": client_id, "client_secret": client_secret, "token": token}
    response = requests.post(keycloak_config["web"]["token_introspection_uri"], data=request_data, headers=headers)
    if response.status_code == 200:
        result = json.loads(response.text)
        if "active" in result and result["active"]:
            if full_resp:
                return {"username": result["preferred_username"], "user_id": result["sub"], "issuer": result["iss"],
                        "email": result["email"], "given_name": result["given_name"],
                        "family_name": result["family_name"]}
            else:
                return {"username": result["preferred_username"] if "preferred_username" in result else None,
                        "user_id": result["sub"] if "sub" in result else None, "issuer": result["iss"]}
        else:
            return None
    else:
        return None


def issue_delegation_token(token, issuer):
    """
    Function to issue a delegation token for another DARE component using keycloak module

    Args
        | token (str): the access token provided by the user
        | issuer (str): the identity provider, i.e. can be dare or any 3rd party identity provider enabled in
        keycloak (e.g. Google, GitHub etc)

    Returns
        | response: http response from keycloak regarding the delegation token
    """
    keycloak_config = load_keycloak_config()
    client_id = os.environ["CLIENT_ID"] if "CLIENT_ID" in os.environ else keycloak_config["web"]["client_id"]
    client_secret = os.environ["CLIENT_SECRET"] if "CLIENT_SECRET" in os.environ else keycloak_config["web"][
        "client_secret"]
    headers = {"content-type": "application/x-www-form-urlencoded"}
    request_data = {"client_id": client_id, "client_secret": client_secret, "subject_token": token,
                    "subject_issuer": issuer, "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange"}
    return requests.post(keycloak_config["web"]["token_uri"], data=request_data, headers=headers)


def refresh_token(ref_token):
    keycloak_config = load_keycloak_config()
    client_id = os.environ["CLIENT_ID"] if "CLIENT_ID" in os.environ else keycloak_config["web"]["client_id"]
    client_secret = os.environ["CLIENT_SECRET"] if "CLIENT_SECRET" in os.environ else keycloak_config["web"][
        "client_secret"]
    headers = {"content-type": "application/x-www-form-urlencoded"}
    request_data = {"client_id": client_id, "client_secret": client_secret, "refresh_token": ref_token,
                    "grant_type": "refresh_token"}
    return requests.post(keycloak_config["web"]["token_uri"], data=request_data, headers=headers)


def authenticate_user(username, password, requested_issuer):
    """
    Uses the keycloak endpoint to authenticate a user. Retrieves configuration inputs from the secrets.json and
    gets the user's credentials from the request.

    Args
        | username (str): the provided username
        | password (str): the provided password

    Returns
        response: the response from keycloak
    """
    try:
        keycloak_config = load_keycloak_config()
        url = keycloak_config["web"]["token_uri"]
        client_id = os.environ["CLIENT_ID"] if "CLIENT_ID" in os.environ else keycloak_config["web"]["client_id"]
        client_secret = os.environ["CLIENT_SECRET"] if "CLIENT_SECRET" in os.environ else keycloak_config["web"][
            "client_secret"]
        headers = {"content-type": "application/x-www-form-urlencoded"}
        data = {"client_id": client_id, "client_secret": client_secret, "username": username, "password": password,
                "grant_type": "password", "requested_issuer": requested_issuer} if requested_issuer != "dare" else \
            {"client_id": client_id, "client_secret": client_secret, "username": username, "password": password,
             "grant_type": "password"}
        response = requests.post(url, data=data, headers=headers)
        return response
    except (FileNotFoundError, Exception) as e:
        print(e)
        return None


def register_user(url, access_token, username, password, first_name, last_name, email):
    data = {
        "username": username,
        "credentials": [{
            "value": password,
            "temporary": False,
            "type": "password"
        }],
        "enabled": True,
        "emailVerified": True,
        "firstName": first_name,
        "lastName": last_name,
        "email": email,
    }
    headers = {
        "content-type": "application/json",
        "Authorization": "bearer {}".format(access_token)
    }
    return requests.post(url, json=data, headers=headers)


def init_dare_services(token, username, logger, password=None):
    if not password:
        password = "Dare_{}_123".format(username)

    # init Dispel4py Registry
    d4p_resp = init_d4p_registry(username, password, token)
    if d4p_resp[0] != 200:
        logger.error("Error while initializing d4p registry")
        logger.error(d4p_resp)
        return d4p_resp[1], d4p_resp[0]

    # init Execution API
    exec_api_resp = init_exec_api(username)
    if exec_api_resp[0] != 200:
        logger.error("Error while initializing exec-api")
        logger.error(exec_api_resp)
        return exec_api_resp[1], exec_api_resp[0]

    # Init CWL Workflow Registry
    workflow_registry_resp = init_workflow_registry(username=username, password=password, token=token)
    if workflow_registry_resp[0] != 200:
        logger.error("Error while initializing cwl workflow registry")
        logger.error(workflow_registry_resp)
        return workflow_registry_resp[1], workflow_registry_resp[0]
    return "Env successfully initialized!", 200


def init_d4p_registry(username, password, token):
    """
    Function to initialize a user in d4p-registry. The API endpoint of d4p-registry checks if the user is new and
    registers him/her in the d4p-registry MySQL database.

    Args
        | username (str): username of the user for the DARE platform
        | password (str): user's password
        | token (str): the access token gained from keycloak when signing in the user

    Returns
        | tuple: response status code and text
    """
    user_data = validate_token(token=token, full_resp=True)
    if not user_data:
        return 500, "Could not validate token"
    data = {
        "username": username,
        "password": password,
        "access_token": token,
        "email": user_data["email"],
        "given_name": user_data["given_name"],
        "family_name": user_data["family_name"]
    }
    headers = {"Content-Type": "application/json"}
    r = requests.post(D4P_URL + '/accounts/keycloak_user/', data=json.dumps(data), headers=headers)
    return r.status_code, r.text


def init_exec_api(username):
    """
    Function to initialize user's workspace in DARE platform through the Execution API

    Args
        | username (str): username of the logged in user

    Returns
        | tuple: response's status code and text
    """
    data = {"username": username}
    url = EXEC_API_URL + '/create-folders'
    r = requests.post(url, data=data)
    return r.status_code, r.text


def init_workflow_registry(username, password, token):
    user_data = validate_token(token=token, full_resp=True)
    if not user_data:
        return 500, "Could not validate token"
    data = {
        "username": username,
        "password": password,
        "access_token": token,
        "email": user_data["email"],
        "given_name": user_data["given_name"],
        "family_name": user_data["family_name"]
    }
    headers = {"Content-Type": "application/json"}
    r = requests.post(WORKFLOW_REGISTRY_URL + '/accounts/keycloak_user/', data=json.dumps(data), headers=headers)
    return r.status_code, r.text

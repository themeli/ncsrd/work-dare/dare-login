flask==1.1.2
requests==2.24.0
sphinx==3.2.1
sphinx-rtd-theme==0.5.0
recommonmark==0.6.0
uwsgi==2.0.19.1
PyYAML==5.3.1
flask-swagger==0.2.14
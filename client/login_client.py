import json
from os.path import join, exists
from os import getcwd

import requests
import yaml


def read_credentials():
    cred_file = join(getcwd(), "credentials.yaml")
    example_cred_file = join(getcwd(), "example_credentials.yaml")
    cred_filename = cred_file if exists(cred_file) else example_cred_file
    with open(cred_filename, "r") as f:
        return yaml.safe_load(f)


def login(hostname, username, password, requested_issuer):
    data = {
        "username": username,
        "password": password,
        "requested_issuer": requested_issuer
    }
    headers = {"Content-Type": "application/json"}
    r = requests.post(hostname + '/auth', data=json.dumps(data), headers=headers)
    if r.status_code == 200:
        response = json.loads(r.text)
        return {"access_token": response["access_token"], "refresh_token": response["refresh_token"],
                "expires_in": response["expires_in"]}
    else:
        print("Could not authenticate user!")


def register(hostname, username, password, first_name, last_name, email):
    url = hostname + "/register"
    data = {
        "username": username,
        "password": password,
        "first_name": first_name,
        "last_name": last_name,
        "email": email
    }
    headers = {"Content-Type": "application/json"}
    register_resp = requests.post(url, data=json.dumps(data), headers=headers)
    return register_resp.status_code, register_resp.text


def validate_token(hostname, token, full_resp=None):
    """
    Function to validate a given token. If the token is valid, the API asks for user info so as to retrieve a
    permanent id. Some of the returned fields (if token is valid) are as presented below:

    - "sub": "cfbdc618-1d1e-4b1d-9d35-3d23f271abe3",
    - "email_verified": true,
    - "name": "Sissy Themeli",
    - "preferred_username": "sthemeli",
    - "given_name": "Sissy",
    - "family_name": "Themeli",
    - "email": "sthemeli@iit.demokritos.gr"

    Args
        request: http request
        oidc: open-id client

    Returns
        str: user's permanent id
    """
    try:
        url = "{}/validate-token".format(hostname)
        data = {"access_token": token}
        if full_resp:
            data["full_resp"] = full_resp
        response = requests.post(url, data=json.dumps(data))
        return response.status_code, response.text
    except (ConnectionError, Exception) as e:
        print(e)
        return 500, "Could not validate token"


def issue_delegation_token(hostname, token, issuer):
    try:
        url = "{}/delegation-token".format(hostname)
        response = requests.post(url, data=json.dumps({"access_token": token, "issuer": issuer}))
        if response.status_code != 200:
            print(response.status_code, response.text)
        else:
            delegation_token_response = json.loads(response.text)
            return delegation_token_response["access_token"]
    except (ConnectionError, Exception) as e:
        print(e)
        return 500, "Could not issue delegation token"


def refresh_token(hostname, refresh_token, issuer):
    try:
        print("Current refresh token: {}".format(refresh_token))
        url = "{}/refresh-token".format(hostname)
        response = requests.post(url, data=json.dumps({"refresh_token": refresh_token, "issuer": issuer}))
        if response.status_code != 200:
            print(response.status_code, response.text)
        refresh_resp = json.loads(response.text)
        print("Access token: {}".format(refresh_resp["access_token"]))
        print("Refresh token: {}".format(refresh_resp["refresh_token"]))
        print("Expires in: {}".format(refresh_resp["expires_in"]))
    except (ConnectionError, Exception) as e:
        print(e)
        return 500, "Could not refresh token! {}".format(e)


def test_login(hostname, creds):
    response = login(hostname=hostname, username=creds["username"], password=creds["password"],
                     requested_issuer=creds["issuer"])
    access_token = response["access_token"]
    refresh_token = response["refresh_token"]
    print(access_token)
    return access_token, refresh_token


def test_validate_token(hostname, access_token):
    # test token validation
    valid_resp = validate_token(hostname=hostname, token=access_token, full_resp=True)
    print("Status code: {}, validation response: {}".format(valid_resp[0], valid_resp[1]))
    return json.loads(valid_resp[1])


def test_delegation_token(hostname, access_token, issuer):
    # test delegation token
    delegation_token = issue_delegation_token(hostname, access_token, issuer)
    print(delegation_token)
    return delegation_token


def test_refresh_token(hostname, ref_token, issuer):
    # test refresh token
    rt = refresh_token(hostname=hostname, refresh_token=refresh_token, issuer=issuer)
    print(rt)


def test_registration(hostname, username, password, first_name, last_name, email):
    regist_status_code, regist_resp = register(hostname=hostname, username=username, password=password,
                                               first_name=first_name, last_name=last_name, email=email)
    print(regist_status_code, regist_resp)


def test(hostname, creds):
    # access_tkn, refresh_tkn = test_login(hostname=hostname, creds=creds)
    # valid_resp = test_validate_token(hostname=hostname, access_token=access_tkn)
    # issuer = valid_resp["issuer"]
    # test_delegation_token(hostname=hostname, access_token=access_tkn, issuer=issuer)
    # test_refresh_token(hostname=hostname, ref_token=refresh_tkn, issuer=issuer)
    test_registration(hostname, username="test", password="test", first_name="test", last_name="test",
                      email="test@test.com")


if __name__ == '__main__':
    # LOGIN_HOSTNAME = "https://testbed.project-dare.eu/dare-login"
    # LOGIN_HOSTNAME = "http://localhost:5001"
    LOGIN_HOSTNAME = "https://platform.dare.scai.fraunhofer.de/dare-login"
    credentials = read_credentials()
    test(hostname=LOGIN_HOSTNAME, creds=credentials)
